package the_fireplace.fulcrum.gui;

import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
/**
 *
 * @author The_Fireplace
 *
 */
public abstract class ModTabs {
	public static ModTabs[] tabArray = new ModTabs[1];
	public static GuiModTab[] guiArray = new GuiModTab[1];
	public static final ModTabs tabfulcrum = new ModTabs(0, "fireplace_core", Items.bed, new GuiFulcrumTab()){};
	private final int tabIndex;
	private final String tabLabel;
	private String tabTexture = "modtab_blank.png";
	private boolean drawTitle = true;
	@SideOnly(Side.CLIENT)
	private ItemStack itemIconStack;
	private Item item;
	private int itemIconDamage = 0;

	public ModTabs(String label, Item icon, GuiModTab gui){
		this(getNextID(), label, icon, gui);
	}

	public ModTabs(String label, Item icon, int itemDamage, GuiModTab gui){
		this(label, icon, gui);
		this.itemIconDamage = itemDamage;
	}

	private ModTabs(int index, String label, Item icon, GuiModTab gui){
		this.item = icon;
		if(index >= tabArray.length){
			ModTabs[] tmp = new ModTabs[index + 1];
			for(int x=0;x<tabArray.length;x++){
				tmp[x]=tabArray[x];
			}
			tabArray=tmp;
		}
		if(index >= guiArray.length){
			GuiModTab[] tmp = new GuiModTab[index + 1];
			for(int x=0;x<guiArray.length;x++){
				tmp[x]=guiArray[x];
			}
			guiArray=tmp;
		}
		this.tabIndex=index;
		this.tabLabel=label;
		tabArray[index]=this;
		guiArray[index]=gui;
	}

	public int getTabIndex(){
		return this.tabIndex;
	}

	public ModTabs setBackgroundImageName(String texture){
		this.tabTexture=texture;
		return this;
	}

	public String getTabLabel(){
		return this.tabLabel;
	}

	public String getTranslatedTabLabel(){
		return "utilTab." + this.getTabLabel();
	}

	@SideOnly(Side.CLIENT)
	public ItemStack getIconItemStack()
	{
		if (this.itemIconStack == null)
		{
			this.itemIconStack = new ItemStack(this.item, 1, this.getIconItemDamage());
		}

		return this.itemIconStack;
	}

	@SideOnly(Side.CLIENT)
	public int getIconItemDamage()
	{
		return itemIconDamage;
	}

	@SideOnly(Side.CLIENT)
	public String getBackgroundImageName()
	{
		return this.tabTexture;
	}

	@SideOnly(Side.CLIENT)
	public boolean drawInForegroundOfTab()
	{
		return this.drawTitle;
	}

	public ModTabs setNoTitle()
	{
		this.drawTitle = false;
		return this;
	}

	@SideOnly(Side.CLIENT)
	public int getTabColumn()
	{
		if (tabIndex > 11)
		{
			return ((tabIndex - 12) % 10) % 5;
		}
		return this.tabIndex % 6;
	}

	@SideOnly(Side.CLIENT)
	public boolean isTabInFirstRow()
	{
		if (tabIndex > 11)
		{
			return ((tabIndex - 12) % 10) < 5;
		}
		return this.tabIndex < 6;
	}

	public int getTabPage()
	{
		if (tabIndex > 11)
		{
			return ((tabIndex - 12) / 10) + 1;
		}
		return 0;
	}

	public static int getNextID()
	{
		return tabArray.length;
	}
}

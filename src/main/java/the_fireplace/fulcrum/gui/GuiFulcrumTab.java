package the_fireplace.fulcrum.gui;

import net.minecraft.util.StatCollector;

/**
 *
 * @author The_Fireplace
 *
 */
public class GuiFulcrumTab extends GuiModTab {
	public GuiFulcrumTab(){
		super();
		fontRendererObj = mc.fontRendererObj;
	}
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		this.drawCenteredString(fontRendererObj, StatCollector.translateToLocal("fulcrum.exit"), this.width / 2, this.height / 2, -1);
		super.drawScreen(mouseX, mouseY, partialTicks);
	}
	@Override
	public void initGui(){
		super.initGui();
	}
}

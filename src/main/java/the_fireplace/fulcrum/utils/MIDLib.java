package the_fireplace.fulcrum.utils;

import net.minecraftforge.fml.common.Loader;

public class MIDLib {
	public static boolean hasAdobeBlocks(){
		return Loader.isModLoaded("adobeblocks");
	}
	public static boolean hasBaseMetals(){
		return Loader.isModLoaded("basemetals");
	}
	public static boolean hasNeoTech(){
		return Loader.isModLoaded("neotech");
	}
	public static boolean hasPowerAdvantage(){
		return Loader.isModLoaded("poweradvantage");
	}
	public static boolean hasRealStoneTools(){
		return Loader.isModLoaded("realstonetools");
	}
	public static boolean hasSteelIndustries(){
		return Loader.isModLoaded("SteelIndustries");
	}
	public static boolean hasUnLogicII(){
		return Loader.isModLoaded("unlogicii");
	}
}

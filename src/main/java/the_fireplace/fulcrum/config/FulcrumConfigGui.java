package the_fireplace.fulcrum.config;

import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.client.config.GuiConfig;
import the_fireplace.fulcrum.Fulcrum;
/**
 *
 * @author The_Fireplace
 *
 */
public class FulcrumConfigGui extends GuiConfig{

	public FulcrumConfigGui(GuiScreen parentScreen) {
		super(parentScreen, new ConfigElement(Fulcrum.config.getCategory(Configuration.CATEGORY_GENERAL)).getChildElements(), Fulcrum.MODID, false,
				false, GuiConfig.getAbridgedConfigPath(Fulcrum.config.toString()));
	}

}
